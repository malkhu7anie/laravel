<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('check',
    [UserController::class, 'check']
)->middleware(['check_token']);

Route::delete('delete',
    [ProductController::class, 'delete']
)->middleware(['check_token']);


/*
 * {
      "email": "ahmed@gmail.com",
      "logged_at": "2021/11/24",
      "expired_at": "2021/11/30",
      "user_role": "super_admin"
    }

    ewogICJlbWFpbCI6ICJhaG1lZEBnbWFpbC5jb20iLAogICJsb2dnZWRfYXQiOiAiMjAyMS8x
    MS8yNCIsCiAgImV4cGlyZWRfYXQiOiAiMjAyMS8xMS8zMCIsCiAgInVzZXJfcm9sZSI6ICJz
    dXBlcl9hZG1pbiIKfQ==

    {
      "email": "temp@gmail.com",
      "password": "hashedpassword_2
    }

    ICAgIHsKICAgICAgICAiZW1haWwiOiAidGVtcEBnbWFpbC5jb20iLAogICAgICAgICJwYXNzd29yZCI6ICJoYXNoZWRwYXNzd29yZF8yIgogICAgfQ==

   {
      "email": "temp@gmail.com",
      "password": "hashedpassword_2",
      "logged_at": "2021/11/24",
      "expired_at": "2021/11/30",
      "user_role": "super_admin",
      "name": "product 4"
    }

    ICAgewogICAgICAiZW1haWwiOiAidGVtcEBnbWFpbC5jb20iLAoicGFzc3dvcmQiOiAiaGFzaGVkcGFzc3dvcmRfMiIsCiAgICAgICJsb2dnZWRfYXQiOiAiMjAyMS8xMS8yNCIsCiAgICAgICJleHBpcmVkX2F0IjogIjIwMjEvMTEvMzAiLAogICAgICAidXNlcl9yb2xlIjogInN1cGVyX2FkbWluIiwKICAgICAgIm5hbWUiOiAicHJvZHVjdCA0IgogICAgfQo=

   {
      "email": "john@gmail.com",
      "logged_at": "2021/11/24",
      "expired_at": "2021/11/30",
      "user_role": "super_admin"
    }

    ICAgewogICAgICAiZW1haWwiOiAiam9obkBnbWFpbC5jb20iLAogICAgICAibG9nZ2VkX2F0IjogIjIwMjEvMTEvMjQiLAogICAgICAiZXhwaXJlZF9hdCI6ICIyMDIxLzExLzMwIiwKICAgICAgInVzZXJfcm9sZSI6ICJzdXBlcl9hZG1pbiIKICAgIH0K




 */
