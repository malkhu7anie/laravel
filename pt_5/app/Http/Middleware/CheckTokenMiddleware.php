<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Facades\File;
use Symfony\Component\HttpFoundation\Response;

class CheckTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    protected $allowed_emails = [
        'mohammed',
        'michael',
        'temp@gmail.com',
        'ahmed@gmail.com'
    ];

    /*
    public function handle(Request $request, Closure $next): Response
    {
        $error = true;
        if (request()->hasHeader('HELL')) {
            $token = request()->header('HELL');
            $payload = json_decode(base64_decode($token), true);
            if ($payload) {
                if (isset($payload['email'])) {
                    if (in_array($payload['email'], $this->allowed_emails)) {
                        $error = false;
                    }
                }
            }
        }
        if ($error) {
            return response()->json([
                'msg' => "error you do not have access to the requested resource."
            ], 401);
        }
        return $next($request);
    }
     */

    public function handle(Request $request, Closure $next): Response
    {
        $error = true;
        if (request()->hasHeader('HELL')) {
            $token = request()->header('HELL');
            $payload = json_decode(base64_decode($token), true);
            if ($payload) {
                 if (isset($payload['email']) && isset($payload['password'])) {
                    $users = json_decode(
                        file_get_contents(
                            base_path() . "/public/users.json"
                        ), true
                    );
                    foreach ($users as $user) {
                        if ($user['email'] == $payload['email'] && $user['password'] == $payload['password']) {
                            $error = false;
                            break;
                        }
                    }
                }
            }
        }
        if ($error) {
            return response()->json([
                'msg' => "error you do not have access to the requested resource."
            ], 401);
        }
        return $next($request);
    }
}
