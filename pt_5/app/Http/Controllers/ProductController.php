<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Facade\File;

/*
    ewogICAgImVtYWlsIjogImFobWVkQGdtYWlsLmNvbSIsCiAgICAicGFzc3dvcmQiOiAiaGFzaGVkcGFzc3dvcmQiLAogICAgIm5hbWUiOiAicDMiLAogICAgIm93bmVyIjogImFobWVkQGdtYWlsLmNvbSIKfQ==
 */

class ProductController extends Controller
{
    public function delete(Request $request)
    {
        $product = json_decode(file_get_contents(base_path() . "/public/products.json"), true);
        // dd($product);
        $payload = json_decode(base64_decode($request->header('HELL')), true);
        for ($i = 0; $i < count($product); $i += 1) {
            if ($product[$i]['name'] == $payload['name'] && $product[$i]['owner'] == $payload['email']) {
                unset($product[$i]);
            }
        }
        // dd($product);
        file_put_contents(
            base_path() . "/public/products.json", json_encode(array_values($product))
        );
        return response()->json([
            'msg' => 'the product has bee deleted'
        ]);
    }
}
