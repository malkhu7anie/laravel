<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function check()
    {
        $token = request()->header('HELL');
        $payload = json_decode(base64_decode($token), true);
        return response()->json([
            'msg' => " --- The user with email: " . $payload['email'] . " is logged in sucessfully ---"
        ]);
    }
}
